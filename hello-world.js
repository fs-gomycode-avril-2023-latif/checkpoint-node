const http = require("http");
const file = require("fs");

const server = http.createServer((req, res) => {
  file.readFile("welcome.txt", (err, data) => {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.write(data);
    return res.end();
  });

  res.setHeader("Content-Type", "text/html");
  res.write("<h1>Hello world</h1>");
  res.end("");
});

server.listen(3000, "localhost", () => {
  console.log("HELLO WORLD");
});
