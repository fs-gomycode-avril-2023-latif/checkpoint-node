var generator = require("generate-password");

function PasswordGenerator() {
  var password = generator.generate({
    length: 10,
    numbers: true,
    symbols: false,
    excludeSimilarCharacters: true,
  });
  return password;
}

console.log(`mot de passe est ${PasswordGenerator()}`);
